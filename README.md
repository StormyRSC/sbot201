SBoT v6.0 for mudclient201
==========================

A RuneScape Classic bot from 2004 with a large number of available scripts
and a few innovative features.

Originally by RichyT and based on a deobfuscated client by Kaitnieks,
this version was provided by antiyou. It uses poor coding style, typical
for the time.

SBoT was originally for mudclient198, but that version was closed source.

If you need to run SBoT scripts, you may be better served with STS203C
or IdleRSC. This is provided for preservation reasons and nostalgia.

Basic usage
-----------

To run a script, type in the chat box:

	/run fire param1@param2@param3

To stop it, press F9.

Script command names don't necessarily match the file name, check each
file for details.

SBoT uses the standard HC.BMP/slword.txt system used by "sleepers" of the
time for solving CAPTCHAs. No OCR is built-in, you must use an external
one like WUSS.

Key bindings
------------

* Up/Down - adjusts camera pitch
* PgUp/PgDown - zoom camera
* Home/End - zoom minimap
* F1 - as normal, enable/disable interlacing
* F3 - toggle displaying nearby players/NPCs
* F9 - stop script
* F11 - toggle graphical rendering
* F12 - toggle AutoLogin

Other features
--------------

* Fog of war is disabled.
* The camera directly follows the player around.
* There is the ability to "ghost" other players and see the world
  from their perspective by right clicking on them.

Changes by Stormy
-----------------

* Added ant build system.
* Removed malware-like "feature" that allowed SBoT admins to control
  your character.
* Disabled code that attempts to download files.
* Fixed font rendering on non-Windows.

![SBoT admins controlling characters](antiyou.jpg)
